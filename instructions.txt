####################### INSTRUCTIONS TO RUN PROGRAM ###############################

1. You need to have Ruby and Ruby gems installed on your computer.
Note: Ruby version used in development is 2.1.5

2. This program is dependent on the built-in csv library, test/unit, as well as nokogiri, open-uri, and net/http gems. Make sure you have them installed on your system before executing this program.

3. Run 'ruby main.rb' in program root.

#####################################################################################