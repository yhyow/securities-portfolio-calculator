require_relative '../modules/equity_portfolio'
require_relative '../modules/brokerage_account'
require_relative '../modules/trade_processor'
require 'test/unit'
require 'csv'

# The focus of these tests are on making sure invalid transactions are flagged
class InvalidTransactionsTest < Test::Unit::TestCase

  def test_short_sell_failure
    assert_raise(RuntimeError) do
      EquityPortfolio.new "transactions_short_sell"
    end
  end

  def test_invalid_price_failure
    assert_raise(RuntimeError) { EquityPortfolio.new "transactions_invalid_price" }
  end

  def test_illegal_trade_action
    assert_raise(RuntimeError) { EquityPortfolio.new "transactions_illegal_action" }
  end

  def test_missing_file
    assert_raise(RuntimeError) { EquityPortfolio.new "transactions_missing" }
  end

end