require_relative '../modules/equity_portfolio'
require_relative '../modules/brokerage_account'
require_relative '../modules/trade_processor'
require 'test/unit'
# require 'minitest/autorun'
require 'csv'

# Very basic tests for realised and unrealised P n L
class ProfitLossTest < Test::Unit::TestCase

  def setup
    @ep = EquityPortfolio.new
  end

  # if you have not sold any stock, your realised profit and loss should be 0.0
  def test_realised_profit_loss_no_sold
    assert_equal 0.0, @ep.get_realised_profit_loss(100.0, [], [])
  end

  # if you have sold shares, but your average cost is the same as your average average sellng price, your realised profit and loss should be 0.0
  def test_realised_profit_loss_with_sold
    assert_equal 0.0, @ep.get_realised_profit_loss(100.0, [-1000], [-10])
  end

  # if you have no balance for a particular stock, your unrealised profit and loss should be 0.0
  def test_unrealised_profit_loss_with_no_stock_balance
    assert_equal 0.0, @ep.get_unrealised_profit_loss(100.0, [0], 'D05.SI')
  end

  # if you have a positive balance of a particular stock, but the latest share price is equal to your average cost price, your balance should be 0.0
  def test_unrealised_profit_loss_with_stock_balance
    assert_equal 0.0, @ep.get_unrealised_profit_loss(15.79, [1], 'D05.SI')
  end

end