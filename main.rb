####----- Authored by YONG HUI-----########
###########################################
require_relative 'modules/brokerage_account'
require_relative 'modules/equity_portfolio'
require_relative 'modules/bond_portfolio'

my_equity_portfolio = EquityPortfolio.new "transactions_general"
# my_bond_portfolio = BondPortfolio.new
# my_regional_equity_portfolio = EquityPortfolio.new("transactions_regional")
# my_global_equity_portfolio = EquityPortfolio.new("transactions_global")
BrokerageAccount.new(my_equity_portfolio).start_new_session
# BrokerageAccount.new(EquityPortfolio.new, BondPortfolio.new, GoldPortfolio.new).start_new_session