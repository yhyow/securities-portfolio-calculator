class Transaction
  attr_accessor :date, 
                :type, 
                :stock_sym,
                :quantity, 
                :yahoo_url, 
                :yahoo_csv, 
                :price, 
                :value
end