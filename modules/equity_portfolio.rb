#### require dependency files ####
require_relative 'portfolio'

class EquityPortfolio < Portfolio

  def initialize(transactions_file="transactions_general")
    super
  end
  
  def get_net_position
    # net_position = []
    # @securities.each { |security| security.get_net_position }
    print "#{net_realised_position},#{net_unrealised_position}\n"
  end
  
  def net_realised_position
    realised_profit_loss 
  end
  
  def net_unrealised_position
    unrealised_profit_loss
  end

end
