require_relative 'constants'
require_relative 'transactions'
require 'csv'
require 'nokogiri'
require 'open-uri'
require 'net/http'

module TradeProcessorHelpers
  ######################################################################
  ###### HERE ARE METHODS WHICH OPERATE ON @transactions ###############
  ###### WHICH IS AN ARRAY OF TRANSACTION OBJECTS ######################
  ######################################################################

  # Documention for method: #select_stock_transactions
  #
  #   a. if and_column is nil, you get array of objects you selected
  #   b. if and_column is not nil, you get an array of the column you pass in
  #   c. if skip_sum is false, which is default, you get back a summation of the column
  #   d. if skip_sum is true, you get back c.
  #   e. the actual summation is delegated to #select_column and #sum_column

  def select_stock_transactions(stock_sym, and_column=nil, skip_sum=false)
    res = transactions.select { |t| t.stock_sym == stock_sym }
    and_column ? select_column(res, and_column, skip_sum) : res
  end

  def select_column(res, column_name, skip_sum=false)
    arr = res.map { |t| t.send(column_name.to_sym) }
    skip_sum ? arr : sum_column(arr, column_name)
  end
  
  def sum_column(col, column_name)
    col.reduce(:+)
  end

  #   NOTE: THE REST OF THESE OPERATIONS ARE BUILT OFF THE ABOVE 3 METHODS
  
  def naive_average_price(k)
    # i use type because i wanted to only use buy purcase as a benchmark
    res = select_type_skip_sum(k)
    sum = select_price(k)
    ( sum  / res.size ).round(2)
  end
  
  def select_type(k)
    select_stock_transactions(k, "type")
  end
  def select_type_skip_sum(k)
    select_stock_transactions(k, "type", true)
  end

  def select_quantity(k)
    select_stock_transactions(k.to_s, "quantity")
  end
  def select_quantity_skip_sum(k)
    select_stock_transactions(k.to_s, "quantity", true)
  end
  
  def select_value(k)
    select_stock_transactions(k.to_s, "value")
  end
  def select_value_skip_sum(k)
    select_stock_transactions(k.to_s, "value", true)
  end
  
  def select_price(k)
    select_stock_transactions(k.to_s, "price")
  end
  def select_price_skip_sum(k)
    select_stock_transactions(k.to_s, "price", true)
  end

  # cummulative_* methods provide a running accumulation of a column. used for checking if we have enough of a stock to sell, and for computing balance of a stock at each transaction
  def cummulative_value(k)
    sum = 0
    select_stock_transactions(k.to_s, "value", true).map { |v| sum += v }
  end
  
  def cummulative_price(k)
    sum = 0
    res = select_stock_transactions(k.to_s, "price", true).map { |v| sum += v }
  end
  
  def cummulative_quantity(k)
    sum = 0
    select_stock_transactions(k.to_s, "quantity", true).map { |v| sum += v }
  end

  # these 3 methods provides finer grained selections for computing realised P n L
  def select_value_sold(k)
    select_stock_transactions(k.to_s, "value", true).select { |v| v < 0 }
  end

  def select_quantity_sold(k)
    select_stock_transactions(k.to_s, "quantity", true).select { |v| v < 0 }
  end
  
  ######################################################################
  ###### HERE ARE METHODS METHODS TO COMPUTE P N L #####################
  ######################################################################

  def get_realised_profit_loss(benchmark_price, value_sold_array, quantity_sold_array)
    pnl = 0
    value_sold_array.each do |value_sold|
      quantity_sold_array.each do |quantity_sold|
        pnl += -value_sold - ( benchmark_price * -quantity_sold )
      end
    end
    pnl.to_f.round(2)
  end

  def get_unrealised_profit_loss(benchmark_price, stock_balance_array, stock_sym)
    balance = stock_balance_array[stock_balance_array.size-1]
    today_price = get_fake_today_price( stock_sym, date_today )
    pnl = ( today_price - benchmark_price ) * balance
    pnl.to_f.round(2)
  end

  ######################################################################
  ###### HERE ARE HELPER METHODS FOR VALIDATIONS ########
  ######################################################################

  # checks if the cummulative quantity at any point is < 0
  def enough_shares?(arr, stock_sym)
    if arr.any? { |v| v < 0 }
      puts
      puts Constants::SHORT_SELL_MESSAGE
      puts
      raise Constants::SHORT_SELL_MESSAGE
    else
      true
    end
  end

  def valid_price?(share_price)
    if share_price == 0.0 || share_price < 0 || share_price.nil?
      puts
      puts Constants::INVALID_PRICE_MESSAGE
      puts
      raise Constants::INVALID_PRICE_MESSAGE
    else
      true
    end
  end
  
  ######################################################################
  ###### ADJUSTS SIGN OF QUANTITY FOR SELL TYPE TRANSACTION ########
  ######################################################################

  # Adjusts quantity to a negative integer if type of transaction is SELL
  def adjust_quantity_sign(transaction_type, quantity)
    case transaction_type
    when "BUY"
      quantity
    when "SELL"
      0-quantity
    else
      puts
      puts Constants::ILLEGAL_ACTION_MESSAGE
      puts
      raise Constants::ILLEGAL_ACTION_MESSAGE
    end
  end
  
  ######################################################################
  ###### PRICE FETCHING METHODS ########
  ######################################################################

  # Fetches adjusted price from yahoo finance using Nokogiri and checks if the price is valid with weekend? we pass stock_sym in so we can check if we already have a csv file
  def get_price_by_scrapping(url, stock_sym)
    doc = Nokogiri::HTML(open(url, 'User-Agent' => Constants::USER_AGENT), nil, "UTF-8")
    res = ''
    doc.css(".yfnc_tabledata1").each_with_index do |link,i|
      res = link.text if i == 6
    end
    #check if price is valid
    valid_price?(res.to_i)
    res.to_f.round(2)
  end
  
  # Assume today is 7/7/2016
  def get_fake_today_price(stock_sym, date)
    url = "https://sg.finance.yahoo.com/q/hp?s=#{ stock_sym }&a=05&b=07&c=2016&d=05&e=07&f=2016&g=d"
    get_price_by_scrapping( url, stock_sym )
  end
  def get_today_price(stock_sym, date)
    url = "https://sg.finance.yahoo.com/q/hp?s=#{ stock_sym }&a=#{ date[2] }&b=#{ date[1] }&c=#{ date[0] }&d=#{ date[2] }&e=#{ date[1] }&f=#{ date[0] }&g=d"
    get_price_by_scrapping( url, stock_sym )
  end

end