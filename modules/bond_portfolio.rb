#### require dependency files ####
require_relative 'portfolio'

class BondPortfolio < Portfolio

  def initialize(transactions_file="transactions_general")
    super
  end
  
  def get_net_position
    print "#{net_realised_position},#{net_unrealised_position}\n"
  end
  
  def net_realised_position
    realised_profit_loss.reduce(:+) # sum of array of net position for each stock
  end
  
  def net_unrealised_position
    unrealised_profit_loss.reduce(:+) # sum of array of net position for each stock
  end

end
