require_relative 'constants'
require_relative 'transactions'
require_relative 'security'
require 'csv'
require 'nokogiri'
require 'open-uri'
require 'net/http'
#######################################################################################
#### THIS MODULE DOES THE HEAVY LIFTING FOR PROCESSING TRADE TRANSACTIONS #############
#######################################################################################

module TradeProcessor
  # parses the transaction csv and creates Transaction objects and stores them in @transactions. eagerly gets prices from yahoo finance. not optimal
  def load_transactions(file_name)
    CSV.foreach( select_file( file_name ), headers: false ) do |row|
      t = Transaction.new
      t.date = row[0].to_s.split('-')
      t.type = row[1]
      t.stock_sym = row[2]
      t.quantity = adjust_quantity_sign( row[1], row[3].to_i )
      t.yahoo_url = "https://sg.finance.yahoo.com/q/hp?s=#{ t.stock_sym }&a=#{ t.date[2] }&b=#{ t.date[1] }&c=#{ t.date[0] }&d=#{ t.date[2] }&e=#{ t.date[1] }&f=#{ t.date[0] }&g=d"
      t.yahoo_csv = "http://real-chart.finance.yahoo.com/table.csv?s=#{ t.stock_sym }&d=5&e=11&f=2016&g=d&a=9&b=13&c=2004&ignore=.csv"
      t.price = get_price_by_scrapping( t.yahoo_url, t.stock_sym )
      t.value = t.quantity * t.price
      @transactions << t
      
    end
  end
  
  def select_file(file_name)
    file = Dir["fixtures/#{ file_name }.csv"][0]
    # raise "empty file chosen" if File.zero?(file)
    if file
      file
    else
      puts
      puts Constants::MISSING_FILE_MESSAGE
      puts
      raise Constants::MISSING_FILE_MESSAGE
    end
  end
  
  def date_today
    Time.now.strftime("%Y-%d-%m").split('-')
  end
  
  # called from included class - EquityPortfolio. populates @securities with unique entries
  def load_portfolio
    @transactions.each do |t|
      unless @securities.map(&:symbol).include? t.stock_sym
        @securities << Security.new(t.stock_sym)
      end
    end
  end
  
  def load_transactions_into_portfolio
    @transactions.each do |t|
      unless @securities.map(&:symbol).include? t.stock_sym
        @securities << Security.new(t.stock_sym)
      end
    end
  end

  def validate_portfolio(check_enough_shares=true)
    # k is the stock symbol which are entries in @securities
    @securities.each do |k|
      # cummulative_quantity builds cummulative quantities for each stock we have and returns an array. we then check each of these arrays if we have enough of it
      enough_shares?(cummulative_quantity(k.symbol), k.symbol) if check_enough_shares
    end
  end
  
  def realised_profit_loss
    realised_profit_loss = []
    @securities.each do |k|
      # to get realised profit and loss for each stock, we need a 1. baseline price, 2. value of each SELL transaction, and 3. quantity sold of each SELL transaction
      realised_profit_loss << get_realised_profit_loss( naive_average_price(k.symbol), select_value_sold(k.symbol), select_quantity_sold(k.symbol) )
    end
    realised_profit_loss.reduce(:+)
  end
  
  def unrealised_profit_loss
    unrealised_profit_loss = []
    @securities.each do |k|
      unrealised_profit_loss << get_unrealised_profit_loss( naive_average_price(k.symbol), cummulative_quantity(k.symbol), k.symbol )
    end
    unrealised_profit_loss.reduce(:+)
  end

end