module Constants
  USER_AGENT = "annotate_google; http://ponderer.org/download/annotate_google.user.js"
  INVALID_PRICE_MESSAGE = "It seems like a share price is invalid. Call 9677-xxxx for help."
  SHORT_SELL_MESSAGE = "You don't have enough shares, and you cannot short-sell. Check your transactions or call 9677-2869 for help."
  ILLEGAL_ACTION_MESSAGE = "You have attempted to execute an illegal action! You can only BUY AND SELL."
  MISSING_FILE_MESSAGE = "Cannot find your file"
end