require_relative 'equity_portfolio'
#####################################################################
#### WELCOME TO YOUR IDA BROKERAGE ACCOUNT ! ########################
#####################################################################
class BrokerageAccount
  attr_accessor :portfolios
  def initialize(*portfolios)
    @portfolios ||= portfolios
  end

  def start_new_session
    # puts "Hello, Welcome to Your New Brokerage Account"
 #    puts "What would you like to do? (Enter number 1 or 2)"
 #    puts "1. Get my net portfolio position"
 #    puts "2. Close my account"
 #    answer = gets.chomp
 #    case answer.to_i
 #    when 1
 #      puts "Retrieving your portfolio ... "
 #      puts "Retrieving your net position ... "
 #      get_net_position
 #    when 2
 #      puts "Okay bye."
 #      return
 #    else
 #      puts "We don't understand what you mean."
 #    end
    get_net_position
  end
  
  def get_net_position
    @portfolios.each { |portfolio| portfolio.get_net_position }
  end

end