#### require dependency files ####
require_relative 'trade_processor'
require_relative 'trade_processor_helpers'

class Portfolio
  include TradeProcessor
  include TradeProcessorHelpers
  attr_accessor :securities, :transactions
  
  def initialize(transactions_file)
    @securities = []
    @transactions = []
    # eagerly loads transactions on instantiation --  not ideal
    load_transactions transactions_file # argument is name of csv file
    load_portfolio
    validate_portfolio # passing true as an argument skips checking cummulative stock quantities
  end
  
end